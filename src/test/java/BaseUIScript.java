import com.codeborne.selenide.Configuration;
import org.testng.annotations.BeforeSuite;

/**
 * Created by Admin on 4/3/2017.
 */
public class BaseUIScript {

    @BeforeSuite
    public void beforeSuite() {
        System.setProperty("webdriver.chrome.driver", "./src/main/resources/chromedriver.exe");
        Configuration.browser = "chrome";
    }

}
