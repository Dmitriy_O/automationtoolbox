import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

/**
 * Created by Admin on 4/3/2017.
 */
public class SimpleUIScript extends BaseUIScript{

    String url = "http://google.com";

    @Test
    public void simpleScript(){
        open(url);
        $(By.name("q")).setValue("Selenide").pressEnter();
    }
}
